<?php
/**
 * @link http://www.tpframe.com/
 * @copyright Copyright (c) 2017 TPFrame Software LLC
 * @author 510974211@qq.com
 */
namespace tpfcore\smssdk;
use tpfcore\base\SdkSms;

use aliyun\Core\Config;
use aliyun\Core\Profile\DefaultProfile;
use aliyun\Core\DefaultAcsClient;
use aliyun\Api\Sms\Request\V20170525\SendSmsRequest;
use aliyun\Api\Sms\Request\V20170525\QuerySendDetailsRequest;

class AliyunSDK extends SdkSms{
	/**
	 * 获取requestCode的api接口
	 * @var string
	 */
	public static $SEND_URL = 'dysmsapi.aliyuncs.com';
	public static $acsClient = null;
	/**
	 * 发送短信
    success:stdClass Object
            (
                [Message] => OK
                [RequestId] => C134222C-FC0A-4783-A9D9-37B2250F080A
                [BizId] => 915705410655285903^0
                [Code] => OK
            )

    fail:stdClass Object
        (
            [Message] => 模板不合法(不存在或被拉黑)
            [RequestId] => 91FD6DF9-2B63-4B6F-913A-952008F41B31
            [Code] => isv.SMS_TEMPLATE_ILLEGAL
        )

	 * @return string
	 */
	public function sendSms($params){

        $default_=[null, null, null, $templateParam = null, $outId = null, $smsUpExtendCode = null];

        $data=$params+$default_;

        list($signName,$templateCode,$phoneNumbers,$templateParam,$outId,$smsUpExtendCode)=$data;

        // 加载区域结点配置
        Config::load();

		// 初始化SendSmsRequest实例用于设置发送短信的参数
        $request = new SendSmsRequest();

        // 必填，设置雉短信接收号码
        $request->setPhoneNumbers($phoneNumbers);

        // 必填，设置签名名称
        $request->setSignName($signName);

        // 必填，设置模板CODE
        $request->setTemplateCode($templateCode);

        // 可选，设置模板参数
        if($templateParam) {
            $request->setTemplateParam(json_encode($templateParam));
        }

        // 可选，设置流水号
        if($outId) {
            $request->setOutId($outId);
        }

        // 选填，上行短信扩展码
        if($smsUpExtendCode) {
            $request->setSmsUpExtendCode($smsUpExtendCode);
        }

        // 发起访问请求
        $acsResponse = static::getAcsClient($this->AppKey,$this->AppSecret)->getAcsResponse($request);

        // 打印请求结果
        // var_dump($acsResponse);

        return $acsResponse;
		
	}

	public static function getAcsClient($AccessKeyId,$AccessKeySecret) {

        //产品名称:云通信流量服务API产品,开发者无需替换
        $product = "Dysmsapi";

        //产品域名,开发者无需替换
        $domain = static::$SEND_URL;

        // TODO 此处需要替换成开发者自己的AK (https://ak-console.aliyun.com/)
        $accessKeyId = $AccessKeyId; // AccessKeyId

        $accessKeySecret = $AccessKeySecret; // AccessKeySecret


        // 暂时不支持多Region
        $region = "cn-hangzhou";

        // 服务结点
        $endPointName = "cn-hangzhou";


        if(static::$acsClient == null) {

            //初始化acsClient,暂不支持region化
            $profile = DefaultProfile::getProfile($region, $accessKeyId, $accessKeySecret);

            // 增加服务结点
            DefaultProfile::addEndpoint($endPointName, $region, $product, $domain);

            // 初始化AcsClient用于发起请求
            static::$acsClient = new DefaultAcsClient($profile);
        }
        return static::$acsClient;
    }

}