<?php
/**
 * @link http://www.tpframe.com/
 * @copyright Copyright (c) 2017 TPFrame Software LLC
 * @author 510974211@qq.com
 * 阿里云推送消息
 */
namespace tpfcore\push;
use \Push\Request\V20160801 as Push;

class PushAliyun{

	private $appKey="24732684";
	private $accessKeyId="LTAIN3WbDLqBmenn";
	private $accessKeySecret="sUjbUjqZny6BGlht6Nffdc1SYYHSke";
	private $request;
	private $client;
	private $config;
	/*
		$config=[
			"appKey"=>"24732684",
			"accessKeyId"=>"LTAIN3WbDLqBmenn",
			"accessKeySecret"=>"sUjbUjqZny6BGlht6Nffdc1SYYHSke",
			"target"=>"ALL",
			"targetValue"=>"user1,user2",
			"deviceType"=>"ALL",
			"pushType"=>"MESSAGE",
			"title"=>"message title",
			"body"=>"message body"
		];
	*/
	public function __construct($config=[]){
		$this->config=$config;
		if(!array_key_exists("appKey", $config) || !array_key_exists("accessKeyId", $config) || !array_key_exists("accessKeySecret", $config) || empty($config['appKey']) || empty($config['accessKeyId']) || empty($config['accessKeySecret'])){
			throw new \Exception("请先配置好参数", 1);
		}

		$this->appKey=$this->config['appKey'];
		$this->accessKeyId=$this->config['accessKeyId'];
		$this->accessKeySecret=$this->config['accessKeySecret'];

		require VENDOR_PATH.'/aliyun_openapi_sdk/bootstrap.php';
		new \aliyun_openapi_sdk\AutoLoaderClass(["push"]);

		$iClientProfile = \DefaultProfile::getProfile("cn-hangzhou", $this->accessKeyId, $this->accessKeySecret);
		$this->client = new \DefaultAcsClient($iClientProfile);
		$this->request = new Push\PushRequest();

		$this->request->setAppKey($this->appKey);
	}
	/**
	*	正式推送
	*/
	public function push(){
		// 推送目标
		$this->pushTarget();

		// 推送配置
		$this->pushConfig();

		// 推送控制
		$pushTime = gmdate('Y-m-d\TH:i:s\Z', strtotime('+3 second'));	//延迟3秒发送
		$this->request->setPushTime($pushTime);
		$expireTime = gmdate('Y-m-d\TH:i:s\Z', strtotime('+1 day'));	//设置失效时间为1天
		$this->request->setExpireTime($expireTime);
		$this->request->setStoreOffline("true"); // 离线消息是否保存,若保存, 在推送时候，用户即使不在线，下一次上线则会收到
		$response = $this->client->getAcsResponse($this->request);
		return $response;
	}

	/**
	*	推送配置，安卓或IOS
	*/
	private function pushConfig(){
		if($this->config['deviceType']=="iOS" || $this->config['deviceType']=="ALL"){
			// 推送配置: iOS
			$this->request->setiOSBadge("5"); // iOS应用图标右上角角标
			$this->request->setiOSMusic("default"); // iOS通知声音
			$this->request->setiOSApnsEnv("DEV");//iOS的通知是通过APNs中心来发送的，需要填写对应的环境信息。"DEV" : 表示开发环境 "PRODUCT" : 表示生产环境
			$this->request->setiOSRemind("false"); // 推送时设备不在线（既与移动推送的服务端的长连接通道不通），则这条推送会做为通知，通过苹果的APNs通道送达一次(发送通知时,Summary为通知的内容,Message不起作用)。注意：离线消息转通知仅适用于生产环境
			$this->request->setiOSRemindBody("iOSRemindBody");//iOS消息转通知时使用的iOS通知内容，仅当iOSApnsEnv=PRODUCT && iOSRemind为true时有效
			$this->request->setiOSExtParameters("{\"k1\":\"ios\",\"k2\":\"v2\"}"); //自定义的kv结构,开发者扩展用 针对iOS设备
		}
		if($this->config['deviceType']=="ANDROID" || $this->config['deviceType']=="ALL"){
			// 推送配置: Android
			$this->request->setAndroidNotifyType("NONE");//通知的提醒方式 "VIBRATE" : 震动 "SOUND" : 声音 "BOTH" : 声音和震动 NONE : 静音
			$this->request->setAndroidNotificationBarType(1);//通知栏自定义样式0-100
			$this->request->setAndroidOpenType("URL");//点击通知后动作 "APPLICATION" : 打开应用 "ACTIVITY" : 打开AndroidActivity "URL" : 打开URL "NONE" : 无跳转
			$this->request->setAndroidOpenUrl("http://www.aliyun.com");//Android收到推送后打开对应的url,仅当AndroidOpenType="URL"有效
			$this->request->setAndroidActivity("com.ali.demo.OpenActivity");//设定通知打开的activity，仅当AndroidOpenType="Activity"有效
			$this->request->setAndroidMusic("default");//Android通知音乐
			$this->request->setAndroidPopupActivity("com.ali.demo.PopupActivity");//设置该参数后启动辅助托管弹窗功能, 此处指定通知点击后跳转的Activity（辅助弹窗的前提条件：1. 集成第三方辅助通道；2. StoreOffline参数设为true
			$this->request->setAndroidPopupTitle($this->config['title']);
			$this->request->setAndroidPopupBody($this->config['body']);
			$this->request->setAndroidExtParameters("{\"k1\":\"android\",\"k2\":\"v2\"}"); // 设定android类型设备通知的扩展属性
		}
	}
	/**
	*	推送目标
	*/
	private function pushTarget(){
		// 推送目标
		$this->request->setTarget($this->config['target']); //推送目标: DEVICE:推送给设备; ACCOUNT:推送给指定帐号,TAG:推送给自定义标签; ALL: 推送给全部
		$this->request->setTargetValue($this->config['targetValue']); //根据Target来设定，如Target=device, 则对应的值为 设备id1,设备id2. 多个值使用逗号分隔.(帐号与设备有一次最多100个的限制)
		$this->request->setDeviceType($this->config['deviceType']); //设备类型 ANDROID iOS ALL.
		$this->request->setPushType($this->config['pushType']); //消息类型 MESSAGE NOTICE
		$this->request->setTitle($this->config['title']); // 消息的标题
		$this->request->setBody($this->config['body']); // 消息的内容
	}
}