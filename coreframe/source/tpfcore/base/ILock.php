<?php
/**
 * @link http://www.tpframe.com/
 * @copyright Copyright (c) 2017 TPFrame Software LLC
 * @author 510974211@qq.com
 */

namespace tpfcore\base;
interface ILock
{
    const EXPIRE = 5;			// 获取占锁时间 5s
    public function getLock($key, $timeout=self::EXPIRE);  //获取锁
    public function releaseLock($key);	//释放锁
}