<?php
// +----------------------------------------------------------------------
// | Author: yaoyihong <510974211@qq.com>
// | Site home: http://www.tpframe.com
// +----------------------------------------------------------------------
namespace app\common\listener;

use tpfcore\Core;
use think\facade\Event;

/**
 * 初始化事件
 */
class InitListener
{
    /**
     * 事件入口
     */
    public function handle()
    {
        if(!file_exists(APP_PATH.'extra/database.php')) return;

        $HookModel  = Core::loadModel("Hook","common","model");
        
        $AddonModel  = Core::loadModel("Addon","common","model");

        $hook_list = $HookModel->field('name,module')->select();
        
        foreach ($hook_list as $k => $v) {

            if(empty($v)) continue;

            $where[DATA_STATUS] = DATA_NORMAL;

            $where['module'] = $v['module'];

            $data = $AddonModel->where($where)->column('module');

            !empty($data) && Event::listen($v['name'], array_map(array(new Core(),"get_addon_class"),$data)[0]);

        }

    }
}
