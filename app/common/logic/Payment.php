<?php
namespace app\common\logic;

use app\common\dao\CategoryDao;
use app\common\model\ModelBase;
/*	
*	支付基类
*/
class Payment extends ModelBase
{
	/*
	* 获取支付配置参数
	* $handle 	发起支付手柄
	* $code 	支付代号
	*/
	public function getPaymentConfig($handle,$code){
		$result=self::getOneObject(['pay_handle'=>$handle,'pay_code'=>$code,'status'=>1],'pay_config');
		if($result){
			return json_decode($result['pay_config'],true);
		}
		return null;
	}
}
?>