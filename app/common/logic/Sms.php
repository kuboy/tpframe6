<?php
// +----------------------------------------------------------------------
// | Author: yaoyihong <510974211@qq.com>
// +----------------------------------------------------------------------
namespace app\common\logic;
use app\common\model\ModelBase;
use \tpfcore\Core;
/**
 * Description of SmsLogic
 *
 * 短信类
 */
class Sms extends ModelBase
{
    private $config;
    
    /**
     * 发送短信逻辑
     * @param int $scene  发送场景
     * @param int $sender  发送人手机号
     * @param int $scene  发送场景
     * @param int $scene  发送场景
     * @param int $scene  发送场景
     * @param int $unique_id  发送人标识
     Core::loadModel("Sms")->sendSms(2,"18323008570",['code'=>2564]);
     */
    public function sendSms($scene=0, $sender="", $params=[], $unique_id=0)
    {
        //找到可用的短信平台
        $sms=self::getOneObject(['status'=>1],"id,mark,config");
        if(!$sms){
            return [40040,'请先配置短信'];
        }
        $sms_config=$sms['config']?json_decode($sms['config'],true):$sms['config'];
        if(!$sms_config){
            return [40040,'请先配置短信'];
        }

        // 获取发送短信场景
        $smsTemp=Core::loadModel("SmsTemplete",'backend')->getOneObject(['send_scene'=>$scene,'sms_id'=>$sms['id']]);             // 发送模板

        $code = !empty($params['code']) ? $params['code'] : rand(1000,9999);

        \think\Session::init();
        // 发送人标识
        if(empty($unique_id)){
            $session_id = session_id();
        }else{
            $session_id = $unique_id;
        }
        $tpl_content=$smsTemp['tpl_content'];   
        //提取发送短信内容
        foreach ($params as $k => $v) {
            $tpl_content = str_replace('${' . $k . '}', $v, $tpl_content);
        }

        if($sender<>'' && preg_match('/1[34578]\d{9}$/',$sender)){   //如果是正常的手机号码才发送
            //发送记录存储数据库
            $PushLog=[
                'type'=>"sms",
                'trigger'=>$sms['mark'],
                'receiver'=>$sender,
                'session_id'=>$session_id,
                'add_time'=>time(),
                'code'=>$code,
                'status'=>0,
                'msg'=>$tpl_content,
                'scene'=>$scene
            ];
            // 发送短信
            $SmsSdk="\\tpfcore\smssdk\\".$sms['mark']."SDK";
            $resp = (new $SmsSdk($sms_config))->sendSms([
                $sms_config['sign'],        // 短信签名
                $smsTemp['sms_tpl_code'],   // 短信模板编号
                $sender,                    // 短信接收者
                $params                     // 短信发送参数
            ]);

            if ($resp->Code == "OK") {
                // 发送成功
                $PushLog['status']=1;
                $this->where(['id'=>$sms['id']])->inc("used_times");
            }else{
                //发送失败, 将发送失败信息保存数据库
                $PushLog['error_msg']=$resp->Message.$resp->Code;
            }
            Core::loadModel("PushLog")->saveObject($PushLog);
            return $resp->Code == "OK"?[0,"发送成功"]:[40041,"发送失败"];
        }else{
           return [40044,'接收手机号不正确['.$sender.']'];
        }
        
    }   
}
