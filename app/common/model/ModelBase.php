<?php
/**
 * ============================================================================
 * 版权所有 2017-2077 tpframe工作室，并保留所有权利。
 * @link http://www.tpframe.com/
 * @copyright Copyright (c) 2017 TPFrame Software LLC
 * ----------------------------------------------------------------------------
 * 这不是一个自由软件！未经本公司授权您只能在不用于商业目的的前提下对程序代码进行修改和使用；
 * 不允许对程序代码以任何形式任何目的的再发布。
 * ============================================================================
 * 模型基类
 */
namespace app\common\model;

use think\Model;
use think\facade\Db;
use tpfcore\helpers\StringHelper;
use tpfcore\web\Cache;
/*	
*	模型基类
*	最基本的增、删、改、查类，所有的模型类都继承基类
*/
class ModelBase extends Model
{

    // 查询对象
    private static $ob_query = null;
    /**
     * 基类初始化
     */
    
    /**
     * 状态获取器
     */ 
    public function getStatusTextAttr()
    {
        
        $status = [DATA_DELETE => '删除', DATA_DISABLE => '禁用', DATA_NORMAL => '启用'];
        
        return $status[$this->data['status']];
    }
    
    /**
     * 保存数据，没有主键就执行添加，有就执行更新
     * @access protected
     * @param stirng|array $data 要操作的数据
     * @param string|array $where 操作的条件
     * @param string|array $field 操作的字段列表
     * @return boolean
     */
    final protected function saveObject($data = [],$field = [], $where = [])
    {
        
        $pk = $this->getPk();

        return empty($data[$pk]) ? $this->addObject($data, true, $field,$pk) : $this->updateObject($where, $data, $field);
    }
    
    /**
     * 新增数据,是否返回自增的主键
     * @access protected
     * @param stirng|array $data 要操作的数据
     * @param boolean $getLastInsID 返回自增主键   默认返回
     * @return boolean
     */
    final protected function addObject($data = [], $getLastInsID = true, $field = [],$pk)
    { 
        $result =  $this->allowField($field)->save($data);

        return $getLastInsID?$this->$pk:$result;

    }
    
    /**
     * 更新数据
     * @access protected
     * @param stirng|array $data 要操作的数据
     * @param string|array $where 操作的条件
     * @return boolean
     */
    final protected function updateObject($where = [], $data = [], $field = [])
    {
        return $this->allowField($field)->update($data,$where);

    }
    
     /**
     * 聚合函数-统计数据
     * @access protected
     * @param string|array $where 操作的条件
     * @param stirng $stat_type 统计类型，默认为count，统计条件
     * @return mixed
     */
    final protected function getStatistics($where = [], $stat_type = 'count', $field = 'id')
    {
        
        return $this->where($where)->$stat_type($field);
    }
    
    /**
     * 保存多个数据到当前数据对象
     * @access public
     * @param array   $data_list 数据
     * @param boolean $replace 是否自动识别更新和写入
     * @return array|false
     */
    final protected function opObjects($data_list = [], $replace = false)
    {
        
        return $this->saveAll($data_list, $replace);
    }
    
    /**
     * 设置某个字段值
     * @access protected
     * @param array   $where 条件
     * @param string $field 字段名
     * @param string $value 新的值
     * @return boolean
     */
    final protected function setFieldValue($where = [], $field = '', $value = '')
    {
        return $this->updateObject($where, [$field => $value]);
    }
    
    /**
     * 删除数据（分真实删除与更改字段状态）
     * @access protected
     * @param array   $where 条件
     * @param string $is_true 是否真实删除
     * @return boolean
     */
    final protected function deleteObject($where = [], $is_true = false , $column = null)
    { 
        return $is_true ? $this->where($where)->delete() : $this->setFieldValue($where, is_null($column)?DATA_STATUS:$column, DATA_DELETE);
    }
    
    /**
     * 得到某个列的数组
     * @access protected
     * @param array   $where 条件
     * @param string $field 字段名 多个字段用逗号分隔
     * @param string $key   索引
     * @return array
     */
    final protected function getColumns($where = [], $field = '', $key = '')
    {
        return Db::name($this->name)->where($where)->column($field, $key);
    }
    
    /**
     * 得到某个字段的值
     * @access protected
     * @param array   $where 条件
     * @param string $field   字段名
     * @param mixed  $default 默认值
     * @param bool   $force   强制转为数字类型
     * @return mixed
     */
    final protected function getColumnValue($where = [], $field = '', $default = null, $force = false)
    {
        return Db::name($this->name)->where($where)->value($field, $default, $force);
    }
    
    /**
    * 查找单条记录
    * @access protected
    * @param array   $where 条件
    * @param string $field   字段名
    * @return mixed
    */
    public function getOneObject($where = [], $field = true)
    {
       return $this->where($where)->field($field)->find();
    }

    /**
    * 获取数据列表
    * @access protected
    * @param array   $param 参数
    * @return mixed
    */
    public function getList($param=[])
    {
        $defaultParam=   [
            "where" =>[],
            "field" =>true,
            "order" =>"",
            "orderRaw"=>"",
            "paginate"  =>['rows' => null, 'simple' => false, 'config' => []],
            "join"      =>['join' => null, 'condition' => null, 'type' => 'INNER'],
            "group"     =>['group' => '', 'having' => ''],
            "limit" =>null,
            "data"  =>null,
            "expire"=>0
        ]; 
        $param=StringHelper::parseStrTable($param);

        $params=array_merge($defaultParam,$param);

        extract($params);

        if(!empty($paginate['rows'])){
            if(is_numeric($paginate['rows']) && $paginate['rows']>0){
                $paginate['rows']=["list_rows"=>$paginate['rows']];
            }

            !empty($paginate['config']) && $paginate['rows'] = array_merge($paginate['rows'],$paginate['config']);
        }

        $paginate['simple'] = empty($paginate['simple']) ? false   : $paginate['simple'];
        
        $paginate['config'] = empty($paginate['config']) ? []      : $paginate['config'];
        
        $join['condition']  = empty($join['condition'])  ? null    : $join['condition'];
        
        $join['type']       = empty($join['type'])       ? 'INNER' : $join['type'];
        
        $group['having']    = empty($group['having'])    ? ''      : $group['having'];
        
        !empty($orderRaw)?self::$ob_query = $this->where($where)->orderRaw($orderRaw):self::$ob_query = $this->where($where)->order($order);

        if(!empty($join['join'])){

            if(is_array($join['join'])){

                foreach ($join['join'] as $key => $value) {

                    self::$ob_query = self::$ob_query->join($join['join'][$key], $join['condition'][$key], $join['type'][$key]);

                }

            }else{

                self::$ob_query = self::$ob_query->join($join['join'], $join['condition'], $join['type']);

            }

        }
        
        self::$ob_query = self::$ob_query->field($field);
        
        !empty($group['group'])   && self::$ob_query = self::$ob_query->group($group['group']);

        !empty($group['having'])  && self::$ob_query = self::$ob_query->having($group['having']);
    
        !empty($limit)            && self::$ob_query = self::$ob_query->limit($limit);
        
        $cache_tag = Cache::get_cache_tag($this->name, $join);
        
        $cache_key = Cache::get_cache_key($this->name, $where, $field, $order, $paginate, $join, $group, $limit, $data);
        
        if (\think\Facade\Cache::has($cache_key) && Cache::check_cache_tag($cache_tag)) {

            return unserialize(\think\Facade\Cache::get($cache_key));
            
        } else {

            $result_data = !empty($paginate['rows']) ? self::$ob_query->paginate($paginate['rows'], $paginate['simple'], $paginate['config']) : self::$ob_query->select($data);

            HTML_CACHE_ON && \think\Facade\Cache::tag($cache_tag)->set($cache_key, serialize($result_data),$expire) && Cache::set_cache_tag($cache_tag,$expire);
            
            return $result_data;
        }
    }
}
?>