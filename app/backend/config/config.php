<?php
//配置文件
return [
    /* 模板布局配置 */
    'template'  =>  [
        'view_path'     =>  './theme/backend/',     //设置模板位置
        'layout_on'     =>  false,
        'layout_name'   =>  'layout'
    ],
    // 消息推送场景
    'push_scene'=>[
        //　短信发送场景
        'sms_send_scene'  => [
            1   =>'用户注册',
            2   =>'用户找回密码',
            3   =>'修改密码',
            4   =>'提现申请',
        ],
        // 邮件发送场景
        'mail_send_scene'=>[
            1   =>'用户注册',
        ],
    ],
    //插件分类
    'addon_class'=>[
        0=>[
            'type'=>"module",
            "name"=>"模块插件"
        ],
        1=>[
            'type'=>"behavior",
            "name"=>"行为插件"
        ],
        2=>[
            'type'=>"behavior_module",
            "name"=>"行为模块插件"
        ]
    ]
];
