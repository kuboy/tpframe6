<?php
// +----------------------------------------------------------------------
// | Author: yaoyihong <510974211@qq.com>
// +----------------------------------------------------------------------

namespace app\backend\model;

use app\common\model\ModelBase;
use \tpfcore\Core;

class Video extends AdminBase
{
    protected $insert = ['datetime','is_check','user_id'];
    protected $update = ['updatetime'];
    protected function setDatetimeAttr($value)
    {
        return time();
    }
    protected function setIsCheckAttr($value)
    {
        return 1;
    }
    protected function setUserIdAttr($value)
    {
        return session("backend_author_sign")['userid'];
    }
    protected function setUpdatetimeAttr($value)
    {
        return time();
    }
}
