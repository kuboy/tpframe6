<?php
// +----------------------------------------------------------------------
// | Author: yaoyihong <510974211@qq.com>
// +----------------------------------------------------------------------

namespace app\backend\logic;
use \tpfcore\Core;

class Video extends AdminBase
{
	public function saveVideo($data){
		$validate=Core::loadValidate($this->name);
		$validate_result = $validate->scene('add')->check($data);
        if (!$validate_result) {    
            return [RESULT_ERROR, $validate->getError(), null];
        }
		$last_id=Core::loadModel($this->name)->saveObject($data);
		if($last_id){
        	return [RESULT_SUCCESS, '操作成功', url('Video/index')];
        }else{
        	return [RESULT_ERROR, '操作失败', url('Video/index')];
        }
	}
	public function delVideo($data){
		return self::saveObject(['isdelete'=>1],$data)?[RESULT_SUCCESS, '删除成功', url('Video/index')]:[RESULT_ERROR, '删除失败', url('Video/index')];
	}
	public function getVideo($data=[]){
		return self::getList($data);
	}
}