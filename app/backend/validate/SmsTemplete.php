<?php
namespace app\backend\validate;

/**
 * 基础验证器
 */
class SmsTemplete extends AdminBase
{
    // 验证规则
    protected $rule = [
        'send_scene'              	=> 'require',
        'sms_tpl_code'              => 'require',
        'tpl_content'              	=> 'require',
        'sms_id'              		=> 'require|regex:\d+',
    ];

    // 应用场景
    protected $scene = [
        'add'  =>  ['sms_id','send_scene','sms_tpl_code','tpl_content'],
    ];
}