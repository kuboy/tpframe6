<?php
namespace app\backend\validate;

/**
 * 基础验证器
 */
class Video extends AdminBase
{
    // 验证规则
    protected $rule = [
        'config'              => 'require',
    ];

    // 应用场景
    protected $scene = [
        'add'  =>  [''],
    ];
}