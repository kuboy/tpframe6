<?php
namespace app\backend\middleware;
use think\facade\Session;
class Auth
{
    public function handle($request, \Closure $next)
    {
        if(!Session::has("backend_author_sign")){
    		header("Location:".url('backend/User/login'));exit;
    	}

        return $next($request);
    }
}