<?php
// +----------------------------------------------------------------------
// | Author: yaoyihong <510974211@qq.com>
// +----------------------------------------------------------------------
namespace app\tcms\controller;
use \tpfcore\Core;
use think\Db;
class Posts extends FrontendBase
{

    public function index()
    {
        $where=["channel"=>1,"isdelete"=>0,"ischeck"=>1];
        if(!empty($this->param['cid'])){
            $where['cateid']=$this->param['cid'];
        }
        $list = Core::loadModel("Posts")->getPosts(["where"=>$where,"paginate"=>["rows"=>5]]);
        return $this->fetch($this->template,[
            "seo"=>["title"=>"文章列表-tcms 演示站"],
            "list"=>$list,
            'listChild'=>Core::loadModel("Category")->getChilds()
        ]);
    }
    public function detail()
    {
        $list = Core::loadModel($this->name)->getPosts(["where"=>["id"=>$this->param['id']]]);
        if(empty($list)){
            $this->tip([-1,"该文章不存在",url('tcms/Index/index')]);    
        }

        return $this->fetch($this->template,[
            'listChild'=>Core::loadModel("Category")->getChilds(),
            'listPostsComments'=>Core::loadModel("PostsComments")->getPostsCommentsList([
                "where"=>["posts_id"=>$this->param['id'],"ischeck"=>1],
                "field"=>"__POSTS_COMMENTS__.*,__USER__.headimg,__USER__.nickname",
                "join"=>["join"=>"__USER__","condition"=>"__USER__.id=__POSTS_COMMENTS__.user_id","type"=>"left"],
                "paginate"=>["rows"=>10]
            ]),
            "list"=>$list[0],
            "seo"=>["title"=>$list[0]->title."-tcms 演示站","keywords"=>$list[0]->keywords,"description"=>$list[0]->abstract]
        ]);
    }

    public function vote(){
        $this->jump(Core::loadModel("PostsComments")->vote($this->param));
    }
}
