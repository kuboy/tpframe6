<?php
// +----------------------------------------------------------------------
// | Author: yaoyihong <510974211@qq.com>
// +----------------------------------------------------------------------
namespace app\tcms\controller;
use \tpfcore\Core;
use think\Db;
class Message extends FrontendBase
{
    public function add()
    {
        $this->jump(Core::loadModel($this->name,"cms","controller")->saveMessage($this->param));
    }
}
