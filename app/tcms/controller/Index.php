<?php
// +----------------------------------------------------------------------
// | Author: yaoyihong <510974211@qq.com>
// +----------------------------------------------------------------------
namespace app\tcms\controller;
use \tpfcore\Core;
use think\Db;
class Index extends FrontendBase
{
    public function index()
    {
        return $this->fetch($this->template,[
        	"seo"=>["title"=>"tpframe tcms 演示站"]
        ]);
    }

    public function about(){
    	return $this->fetch($this->template,[
            "seo"=>["title"=>"关于我们-tcms 演示站"]
        ]);
    }
    public function product(){
    	return $this->fetch($this->template,[

        ]);
    }

    public function area(){
    	return $this->fetch($this->template,[

        ]);
    }

    public function news(){
    	return $this->fetch($this->template,[

        ]);
    }

    public function contact(){
    	return $this->fetch($this->template,[

        ]);
    }

    public function quotedprice(){
        return $this->fetch("quotedprice",[

        ]);
    }
    public function caselist(){
        return $this->fetch($this->template,[

        ]);
    }
    public function team(){
        return $this->fetch($this->template,[

        ]);
    }
    public function service(){
        return $this->fetch($this->template,[

        ]);
    }
    public function application(){
        return $this->fetch($this->template,[

        ]);
    }
    public function solutions(){
        return $this->fetch($this->template,[

        ]);
    }
    public function catalog(){
        return $this->fetch($this->template,[

        ]);
    }
    public function mount(){
        return $this->fetch($this->template,[

        ]);
    }
    public function careers(){
        return $this->fetch($this->template,[

        ]);
    }
    public function cases(){
        $listPosts= Core::loadModel("Posts")->getPosts(["where"=>["channel"=>2]]);

        $posts_ids = [];
        foreach ($listPosts as $key => $value) {
            $posts_ids[]=$value['id'];
        }

        $listPostsImages = Core::loadModel("PostsImages","cms","logic")->getPostsImages(["where"=>["posts_id"=>["in",implode(",", $posts_ids)]]]);

        return $this->fetch("cases",[
            "listPosts"=>$listPosts,
            "listPostsImages"=>$listPostsImages
        ]);
    }
    public function apply(){
        return $this->fetch($this->template,[

        ]);
    }
    public function buy(){
        return $this->fetch($this->template,[

        ]);
    }
    public function help(){
        return $this->fetch($this->template,[

        ]);
    }
    public function nofound(){
        return $this->fetch($this->template,[

        ]);
    }
    public function pricing(){
        return $this->fetch($this->template);
    }
    public function login(){
        return $this->fetch($this->template);
    }
    public function register(){
        return $this->fetch($this->template);
    }

    public function upgrade(){
        return $this->fetch($this->template);
    }
}