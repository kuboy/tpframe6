<?php
// +----------------------------------------------------------------------
// | Author: yaoyihong <510974211@qq.com>
// +----------------------------------------------------------------------
namespace app\tcms\controller;
use \tpfcore\Core;
use think\Db;
class Shop extends FrontendBase
{
    public function index()
    {
        return $this->fetch($this->template,[
        	
        ]);
    }

    public function detail(){
    	return $this->fetch($this->template,[
        	
        ]);
    }

    public function cart(){
        return $this->fetch($this->template,[
            
        ]);
    }

    public function balance(){
        return $this->fetch($this->template,[
            
        ]);
    }
}
