<?php
// +----------------------------------------------------------------------
// | Author: yaoyihong <510974211@qq.com>
// +----------------------------------------------------------------------
namespace app\tcms\controller;
use \tpfcore\Core;
use think\Db;
class Comment extends Auth
{
	/**
     * @logic Comment,Posts
     * @model Posts
     * @validate 
     */
    public function add()
    {
        $this->jump(Core::loadModel("PostsComments","cms","controller")->savePostsComments($this->param));
    }
    /**
     * @logic Comment,Auth
     * @model Posts
     * @validate 
     */
    public function del()
    {
        $this->jump(Core::loadModel("PostsComments","cms","controller")->savePostsComments($this->param));
    }
}
