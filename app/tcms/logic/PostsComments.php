<?php
namespace app\tcms\logic;
use tpfcore\Core;

class PostsComments extends FrontendBase
{

    public function getPostsComments($data){
        $comments=$this->getPostsCommentsChilds(0,$data);
        return $comments;
    }
    private function getPostsCommentsChilds($pid,$data){
        $children_comments="";$k=0;
        $listPostsComments=self::getList(["where"=>["pid"=>$pid,"posts_id"=>$data['id'],"ischeck"=>1]]);
        foreach ($listPostsComments as $key => $value) {
            $reply=empty($value['reply_author'])?"":'回复 <a href="#comment-'.$value['id'].'">'.$value['reply_author'].'</a>';
            // 查询是否还有子评论
            $listChild=self::getList(["where"=>['pid'=>$value['id']]]);
            if(empty($listChild)){
                $children_comments.=
                '<li class="comments" id="li-comment-'.$value['id'].'">
                    <div id="comment-'.$value['id'].'" class="comment-body">
                        <img src="http://pete.qiniudn.com/avatar/6ca9c52ce1e0dc011795a18c4ab9ab42-104" class="avatar avatar-104" height="104" width="104">
                        <div class="comment-head">
                            <span class="name"><a href="'.$value['url'].'" rel="external nofollow" class="url">'.$value['author'].'</a></span> '.$reply.'
                            <span class="date"> '.date('Y-m-d H:i:s',$value['datetime']).'</span>
                            <span class="floor"></span>
                            <a rel="nofollow" class="comment-reply-link" href="'.url('blog/index/reply').'?replytocom=48#respond" onclick="return addComment.moveForm(\'comment-'.$value['id'].'\', \''.$value['id'].'\', \'respond\', \''.$data['id'].'\' )" aria-label="回复给'.$value['author'].'">[REPLY]</a> </div>
                        <div class="comment-entry">
                            <p>'.$value['comment'].'</p>
                        </div>
                    </div>
                </li>';
            }else{
                $children_comments.=
                '<li class="comments" id="li-comment-'.$value['id'].'">
                    <div id="comment-'.$value['id'].'" class="comment-body">
                        <img src="http://pete.qiniudn.com/avatar/6ca9c52ce1e0dc011795a18c4ab9ab42-104" class="avatar avatar-104" height="104" width="104">
                        <div class="comment-head">
                            <span class="name"><a href="'.$value['url'].'" rel="external nofollow" class="url">'.$value['author'].'</a></span> '.$reply.'
                            <span class="date"> '.date('Y-m-d H:i:s',$value['datetime']).'</span>
                            <span class="floor"></span>
                            <a rel="nofollow" class="comment-reply-link" href="'.url('blog/index/reply').'?replytocom=48#respond" onclick="return addComment.moveForm(\'comment-'.$value['id'].'\', \''.$value['id'].'\', \'respond\', \''.$data['id'].'\' )" aria-label="回复给'.$value['author'].'">[REPLY]</a> </div>
                        <div class="comment-entry">
                            <p>'.$value['comment'].'</p>
                        </div>
                    </div>
                    <ul class="children">';
                $children_comments.=$this->getPostsCommentsChilds($value['id'],$data);
                $children_comments.='
                    </ul>
                </li>';
            }
        }
        return $children_comments;
    }
    public function savePostsComments($data){
    	$validate=Core::addonValidate($this->name);
        $validate_result = $validate->scene("add")->check($data);
        if (!$validate_result) {
            return [1, $validate->getError(), null];
        }

        $result=Core::loadModel($this->name)->addObject($data);
        
        if($result){
        	return [0,"操作成功",null];
        }else{
        	return [1,"操作失败",null];
        }
    }
    public function getPostsCommentsList($where=[]){
        return self::getList($where);
    }   
    public function delPostsComments($data){
        $result=self::deleteObject($data,true);
        if($result){
            return [RESULT_SUCCESS,"操作成功",null];
        }else{
            return [RESULT_ERROR,"操作失败",null];
        }
    }
    // 统计评论数，只统计0
    public function getCountComments($where=[]){
        return self::getStatistics($where);
    }
    public function vote($data=[]){
        if($data['action']=="up"){
            $result = Core::loadModel($this->name,"cms","logic")->where(["id"=>$data["pcid"]])->inc("voteup");
        }else{
            $result = Core::loadModel($this->name,"cms","logic")->where(["id"=>$data["pcid"]])->dec("votedown");
        }
        if($result){
            return [0,"操作成功"];
        }
        return [-1,"操作失败"];
    }
}