<?php
// +----------------------------------------------------------------------
// | Author: yaoyihong <510974211@qq.com>
// +----------------------------------------------------------------------

namespace app\tcms\logic;

use app\common\logic\LogicBase;

/**
 * 前台基础逻辑
 */
class FrontendBase extends LogicBase
{
}